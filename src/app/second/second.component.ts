import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent implements OnInit {
headers: any[]=[
{ header:["Code","Name","Gender","Annual Salary","DateOf Birth"] },
{ header1:["Code11","Name1","Gender1","Annual Salary1","DateOf Birth1"] }
];
headername: any[]=this.headers[0].header;
  employees: any[] = [    
        {    
            code: '1001', name: 'Satya', gender: 'Male',    
            annualSalary: 5500, dateOfBirth: '25/6/1988'    
        },    
        {    
            code: '1002', name: 'Mohit', gender: 'Male',    
            annualSalary: 5700.95, dateOfBirth: '9/6/1982'    
        },    
        {    
            code: '1003', name: 'Rohit', gender: 'Male',    
            annualSalary: 5900, dateOfBirth: '12/8/1979'    
        },    
        {    
            code: '1004', name: 'Satyaprakash Samantaray', gender: 'Female',    
            annualSalary: 6500.826, dateOfBirth: '14/10/1980'    
        }, 
        {    
            code: '1004', name: 'Satyaprakash Samantaray', gender: 'Female',    
            annualSalary: 6500.826, dateOfBirth: '14/10/1980'    
        },    
        {    
            code: '1004', name: 'Satyaprakash Samantaray', gender: 'Female',    
            annualSalary: 6500.826, dateOfBirth: '14/10/1980'    
        }, 
        {    
            code: '1004', name: 'Satyaprakash Samantaray', gender: 'Female',    
            annualSalary: 6500.826, dateOfBirth: '14/10/1980'    
        }, 
        {    
            code: '1004', name: 'Satyaprakash Samantaray', gender: 'Female',    
            annualSalary: 6500.826, dateOfBirth: '14/10/1980'    
        }, 
        {    
            code: '1004', name: 'Satyaprakash Samantaray', gender: 'Female',    
            annualSalary: 6500.826, dateOfBirth: '14/10/1980'    
        }, 
        
    ];    


  constructor() { }

  ngOnInit() {
  }

}
