import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-compo',
  templateUrl: './my-compo.component.html',
  styleUrls: ['./my-compo.component.css']
})
export class MyCompoComponent implements OnInit {
model: any = {};

  onSubmit() {
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.model))
  }

  constructor() { }

  ngOnInit() {
  }

}
