import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyCompoComponent } from './my-compo/my-compo.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatRippleModule } from '@angular/material';

import { SecondComponent } from './second/second.component';
import { ThirdComponent } from './third/third.component';
import { FourthComponent } from './fourth/fourth.component';
import { NavigationComponent } from './navigation/navigation.component';
import { FirstComponent } from './first/first.component';
import { HomeComponent } from './home/home.component';

import { MaterialModule } from './material/material.module';
import { FormControl, FormGroup, Validators, FormsModule } from '@angular/forms';
const appRoutes: Routes=[
{ path:'',component:HomeComponent,pathMatch: 'full'  },

{ path:'mycomp',component:MyCompoComponent,data:{title:'My First Component'} },

{ path:'second',component:SecondComponent,data:{title:'My Second Component'} },
{ path:'third',component:ThirdComponent,data:{title:'My FiThird Component'} },
{ path:'fourth',component:FourthComponent,data:{title:'My Fourth Component'} }
];

@NgModule({
    declarations: [
    AppComponent,
    MyCompoComponent,
    SecondComponent,
    ThirdComponent,
    FourthComponent,
    NavigationComponent,
    FirstComponent,
    HomeComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatRippleModule,
    MaterialModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      { useHash: true }
    ),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
